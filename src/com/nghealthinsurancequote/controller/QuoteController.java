/**
 * 
 */
package com.nghealthinsurancequote.controller;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import java.text.NumberFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Ashutosh
 *
 */
@Controller
public class QuoteController {
	private final Logger logger = LoggerFactory.getLogger(QuoteController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("quote");
		return mav;
	}

	@RequestMapping(value = "/calculate", method = RequestMethod.POST)
	public ModelAndView calculate(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("quote");

		String gender = request.getParameter("gender");
		String age = request.getParameter("age");

		String hypertension = request.getParameter("hypertension") == null ? "false" : "true";
		String bloodpressure = request.getParameter("bloodpressure") == null ? "false" : "true";
		String bloodsugar = request.getParameter("bloodsugar") == null ? "false" : "true";
		String overweight = request.getParameter("overweight") == null ? "false" : "true";

		String smoking = request.getParameter("smoking") == null ? "false" : "true";
		String alcohol = request.getParameter("alcohol") == null ? "false" : "true";
		String dailyexercise = request.getParameter("dailyexercise") == null ? "false" : "true";
		String drugs = request.getParameter("drugs") == null ? "false" : "true";

		float basePremium = 5000;
		String mr = "";
		final float fixedbasePremium = 5000;// fixed

		if (Integer.parseInt(age) >= 18 || Integer.parseInt(age) <= 40) {
			basePremium += (0.1 * fixedbasePremium);// 10% +
		} else {
			basePremium += (0.2 * fixedbasePremium);// 20% +
		}
		if (gender.equals("male")) {
			mr = "Mr.";
			basePremium += (0.02 * fixedbasePremium);// 2% +
		}
		if (hypertension.equals("true")) {
			basePremium += (0.01 * fixedbasePremium);// 1% +
		}
		if (bloodsugar.equals("true")) {
			basePremium += (0.01 * fixedbasePremium);// 1% +
		}
		if (bloodpressure.equals("true")) {
			basePremium += (0.01 * fixedbasePremium);// 1% +
		}
		if (overweight.equals("true")) {
			basePremium += (0.01 * fixedbasePremium);// 1% +
		}
		if (dailyexercise.equals("true")) {
			basePremium -= (0.03 * fixedbasePremium); // 3% -
		}

		if (smoking.equals("true")) {
			basePremium += (0.03 * fixedbasePremium);// 3% +
		}
		if (alcohol.equals("true")) {
			basePremium += (0.03 * fixedbasePremium);// 3% +
		}
		if (drugs.equals("true")) {
			basePremium += (0.03 * fixedbasePremium);// 3% +
		}

		NumberFormat nf = NumberFormat.getInstance();
		mav.addObject("message", "Health Insurance Premium for " + mr + "" + request.getParameter("userName") + ": Rs."
				+ nf.format(basePremium));
		return mav;
	}

	@RequestMapping(value = "/quote", method = RequestMethod.GET)
	public ModelAndView showRegister(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("quote");
		// mav.addObject("message", "Enter value to quote the pricex !");
		return mav;
	}
}
