<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
	integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
	crossorigin="anonymous">
</head>
<body>
	
	
	 
	
	
	
	<div class="form-row">
<div class="form-group col-md-2">

</div>
	 <div class="form-group col-md-8">
	 
	 
<div class="card">
  <div class="card-header">
    Quote Calculation Form
  </div>
  <div class="card-body">
    <h4 class="card-title center"><center>
    
  
<c:if test="${not empty message}"> 
   
   <div class="alert alert-warning alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  ${message}
</div>
</c:if>
</h4>

   
     	<form action="calculate" method="POST" id="needs-validation" novalidate>
			<fieldset class="container">
				<div class="form-row">

					<div class="form-group col-md-12">
						<label for="inputEmail4" class="col-form-label"><strong>Name</strong></label> <input
							type="text" name="userName" class="form-control" id="inputEmail4"
							placeholder="Name" required> 
					</div>
					 

				</div>
				 <div class="form-row">
				
				<div class="form-group col-md-6">
						<label  class="col-form-label"><strong>Gender</strong></label><br />
						<div class="form-check form-check-inline" id="inlineRadio11">
							<label class="custom-control custom-radio"> 
							<input class="custom-control-input" type="radio" name="gender"
								id="inlineRadio1" value="male" required> 
								 <span class="custom-control-indicator"></span>
								  <span class="custom-control-description">Male</span>
							</label>
						</div>
						<div class="form-check form-check-inline">
							<label class="custom-control custom-radio"> 
							<input class="custom-control-input" type="radio" name="gender"
								id="inlineRadio2" value="female" required>
								 <span class="custom-control-indicator"></span>
								 <span class="custom-control-description"> Female </span>
							</label>
						</div>
						<div class="form-check form-check-inline">
							<label class="custom-control custom-radio"> 
							<input  class="custom-control-input" type="radio" name="gender"
								id="inlineRadio3" value="other" required>
								 <span class="custom-control-indicator"></span>
								  <span class="custom-control-description"> Other </span>
							</label>
						</div>
					</div>
				 <div class="form-group col-md-6">
						<label for="inputZip" class="col-form-label"><strong>Age</strong></label>
						 <input   type="number" min="1"  max="110" step="1" class="form-control" name="age" required>
					</div> 

				</div>
				 <div class="form-group">
					<label for="inputAddress" class="col-form-label"><strong>Current Health</strong></label>
					<br/>
					<div class="form-check form-check-inline" id="inputAddress">
					 	<label class="custom-control custom-checkbox">
						  <input type="checkbox" name="hypertension" class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Hypertension</span>
						</label>
						
							<label class="custom-control custom-checkbox">
						  <input type="checkbox"  name="bloodpressure"  class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Blood pressure</span>
						</label>
						
							<label class="custom-control custom-checkbox">
						  <input type="checkbox" name="bloodsugar" class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Blood sugar</span>
						</label>
						
						<label class="custom-control custom-checkbox">
						  <input type="checkbox" name="overweight" class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Overweight</span>
						</label>
					</div>
				</div>
				 <div class="form-group">
					<label for="inputAddress" class="col-form-label"><strong>Habits</strong></label>
					<br/>
					<div class="form-check form-check-inline" id="inputAddress">
					 	<label class="custom-control custom-checkbox">
						  <input type="checkbox" name="smoking" class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Smoking</span>
						</label>
						
							<label class="custom-control custom-checkbox">
						  <input type="checkbox"  name="alcohol"  class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Alcohol</span>
						</label>
						
							<label class="custom-control custom-checkbox">
						  <input type="checkbox" name="dailyexercise" class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Daily exercise</span>
						</label>
						
						<label class="custom-control custom-checkbox">
						  <input type="checkbox" name="drugs" class="custom-control-input">
						  <span class="custom-control-indicator"></span>
						  <span class="custom-control-description">Drugs</span>
						</label>
					</div>
				</div>
				 <button type="submit" class="btn btn-primary btn-lg">Submit</button> 
				 <a href="/NGhealthInsuranceQuote/quote" class="btn btn-secondary btn-lg" role="button" aria-pressed="true">Reset</a>
			</fieldset> 
		</form>
	<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
 
(function() {
  "use strict";
  window.addEventListener("load", function() {
    var form = document.getElementById("needs-validation");
    form.addEventListener("submit", function(event) {
      if (form.checkValidity() == false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add("was-validated");
    }, false);
  }, false);
}());
</script>
  </div>
</div>
	  
	
	</div>
	<div class="form-group col-md-2">
	     
	</div>
	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
		integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
		integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
		crossorigin="anonymous"></script>
</body>
</html>